//menu toggle script
    $('.menu-btn').click(function(){
      $('.navbar .menu').toggleClass("active");
      $('.menu-btn i').toggleClass("active");
  });

//slide-up script 
  $('#back-to-top').click(function(){
      $('html').animate({scrollTop :0});
  });

//active button
$(document).on('click','ul li', function(){
  $(this).addClass('active').siblings().removeClass('active');
});

// OWL CAROUSEL
$(document).ready(function() {
  $(".slider").owlCarousel({
      items : 3,
      dots: false,
      itemsDesktop:[1199,3],
      itemsDesktopSmall:[980,2],
      itemsMobile : [600,1],
      // navigation:true,
      // navigationText:["",""],
      // pagination:true,
      nav: true,
      navText:['<i class="fas fa-long-arrow-alt-left"></i>','<i class="fas fa-long-arrow-alt-right"></i>'],
      autoPlay:true
  });
});